AllMyContracts
==============

This is the main repository with the source code of the online web application
AllMyContracts that allows to manage personal contracts such as term of services
and other agreements.

The project is still in its early phase and only few initial ideas are defined.

To contribute to this project you can either create a ticket in the issue
`tracking system <https://gitlab.com/allmycontracts/allmycontracts/issues/new>`_.

You can for instance propose a feature or suggest changes to the software.

You can also submit 
`Merge Requests <https://gitlab.com/allmycontracts/allmycontracts/-/merge_requests/new>`_.

The assets in this repository is shared with the license described in the ``LICENSE`` file.
